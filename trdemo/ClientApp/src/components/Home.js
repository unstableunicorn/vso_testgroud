import React, { Component } from 'react';
import geodock from '../images/geodock-med.png';

import './Home.css';

export class Home extends Component {
  static displayName = Home.name;

  render() {
    return (
      <div>
        <h1>Welcome Tech Retreat 2019!!!</h1>
        <p>Say hello to my little docker demo!</p>
        <body width="device-width">
          <img src={geodock} className={"geodock"} />
        </body>
      </div>
    );
  }
}
